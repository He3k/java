import java.io.File;
import java.io.IOException;

public class CreateFileDemo {
    public static void main(String[] args) throws IOException {
        File f = new File("test.txt");
        boolean exist = f.createNewFile();
        if (exist)
            System.out.println("Success!");
        else System.out.println("Error");
    }
}
