import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MyWindowApp extends JFrame {
    private int voron = 9;
    private JLabel countLabel;
    private JButton addCrow;
    private JButton removeCrow;
    private JButton killall;

    public MyWindowApp(){
        super("Crow calculator");
        setBounds(400, 400, 800, 800);
        //Подготавливаем компоненты объекта
        countLabel = new JLabel("Crows:" + voron);
        addCrow = new JButton("Add Crow");
        removeCrow = new JButton("Remove Crow");
        killall = new JButton("Kill all");

        //Подготавливаем временные компоненты
        JPanel buttonsPanel = new JPanel(new FlowLayout());
        //Расставляем компоненты по местам
        buttonsPanel.add(countLabel, BorderLayout.NORTH); //О размещении компонент поговорим позже

        buttonsPanel.add(addCrow);
        buttonsPanel.add(removeCrow);
        buttonsPanel.add(killall);

        add(buttonsPanel, BorderLayout.SOUTH);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        addCrow.addActionListener(new ActionListener() {  // add voron
            public void actionPerformed(ActionEvent e) {
                voron = voron + 1;
                countLabel.setText("Crows: " + voron);
            }
        });

        removeCrow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                voron = voron - 1;
                countLabel.setText("Crows: " + voron);
            }
        });

        killall.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                voron = 0;
                countLabel.setText("Crows: " + voron);
            }
        });
    }

    public static void main(String[] args) {
        MyWindowApp app = new MyWindowApp();
        app.setVisible(true);
        //app.pack(); //Эта команда подбирает оптимальный размер в зависимости от содержимого окна
    }
}

//public class MyWindowApp {
//}
