import java.io.*;

public class inputDemo {
    public static void main(String[] args) throws IOException {

        PrintStream sysOut =
                new PrintStream(new FileOutputStream("Output.log"));
        System.setOut(sysOut);

        Process proc = Runtime.getRuntime().exec("tree");
        InputStream input = proc.getInputStream();
        String str = new String((input.readAllBytes()));

        System.out.println(str);

    }
}