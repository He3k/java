import java.io.*;

public class fileOutDemo {
    public static void main(String[] args) throws FileNotFoundException {

            PrintStream errOut =
                    new PrintStream(new FileOutputStream("Error.log"));
            System.setErr(errOut);

            PrintStream sysOut =
                    new PrintStream(new FileOutputStream("Debug.log"));
            System.setOut(sysOut);


        System.out.println("Message to normal work");
        System.err.println("Message to error");

    }
}
