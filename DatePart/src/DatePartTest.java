import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;



public class DatePartTest {
    static public void main(String[] args) {
        Date d = new Date();
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(d);
        System.out.println(d);
        System.out.println("Year: " + gc.get(gc.YEAR));
        System.out.println("Month: " + gc.get(gc.MONTH));
        System.out.println("Day: " + gc.get(gc.DATE));
    }
}