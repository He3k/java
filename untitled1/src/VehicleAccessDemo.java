class Vehicle {
    int passengers;
    int wheels;
    private int max_speed;
    int burn_up;

    Vehicle() {
        this.passengers = 5;
        this.wheels = 4;
        this.max_speed = 100;
        this.burn_up = 9;
    }

    // constuctor vehicle
    Vehicle(int passengers, int wheels, int max_speed, int burn_up) {
        this.passengers = passengers;
        this.wheels = wheels;
        this.max_speed = max_speed;
        this.burn_up = burn_up;
    }

    public void setMax_speed(int max_speed) {
        this.max_speed = max_speed;
    }

    public int getMax_speed() {
        return max_speed;
    }

    // computing distance
    double distance(double interval) {
        double val = this.max_speed * interval;
        return val;
    }

    public String toString() {
        return "passengers = " + passengers +
                ", wheels = " + wheels +
                ", max_speed = " + max_speed +
                ", burn_up = " + burn_up;
    }
}

class Auto extends Vehicle {
    boolean sunroof;

    Auto() {
        this.sunroof = false;
    }

    Auto(int passengers, int wheels, int max_speed, int burn_up, boolean sunroof) {
        super(passengers, wheels, max_speed, burn_up);
        this.sunroof = sunroof;
    }

    public String toString() {
        return  "passengers = " + passengers +
                ", wheels = " + wheels +
                ", max_speed = " + getMax_speed() +
                ", burn_up = " + burn_up +
                ", sunroof = " + sunroof;
    }
}

public class VehicleAccessDemo {
    public static void main(String[] args) {
        Auto bmw = new Auto(4, 4, 180, 12, true);
        Vehicle ferrari = new Vehicle();

        Vehicle[] pvd = {ferrari, bmw};

        for (int i = 0; i < pvd.length; i++) {
            System.out.println(pvd[i].toString());
        }
    }
}
