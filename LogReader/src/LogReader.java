import java.io.*;

public class LogReader {
    public static void main(String[] args) throws IOException, InterruptedException {
        if (args.length == 0) {
            System.out.println("Idi naxui, gde arguments?");
            System.exit(0);
        }
        String logfile = null;
        BufferedReader br = null;
        logfile = args[0]; // ../../../text.txt
        br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(logfile)
                )
        );
        while (true) {
            String line = br.readLine();
            if (line != null) {
                System.out.println(line);
            } else {
                Thread.sleep(500);
            }
        }
    }
}
