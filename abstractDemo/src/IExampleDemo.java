interface A {
    void methodA();
}
interface B extends A {
    void methodB();
}

class IExample implements B {
    public void methodA() { System.out.println("Met A"); }
    public void methodB() { System.out.println("Met B"); }
}

public class IExampleDemo {
    public static void main(String[] args) {
        IExample ie = new IExample();
        ie.methodA();
        ie.methodB();
    }
}
