class Vehicle {
    int passengers;
    int max_speed;
    int wheels;
    Vehicle() {
        passengers = 5;
        max_speed = 90;
        wheels = 4;
    }
    Vehicle(int passengers, int max_speed, int wheels) {
        this.passengers = passengers;
        this.max_speed = max_speed;
        this.wheels = wheels;
    }

    public String toString() {
        return "Vehicle";
    }
}

class Auto extends Vehicle {
    boolean sunroof;
    Auto() {
        sunroof = false;
    }
    Auto(int passengers, int max_speed, int wheel, boolean sunroof) {
        super(passengers, max_speed, wheel);
        this.sunroof = sunroof;
    }

    public String toString() {
        return "Auto";
    }
}

public class InstanceOfDemo {
    public static void main(String[] args) {
        Auto a = new Auto();
        Vehicle v = new Vehicle();

        Vehicle[] va = {v, a};
        for(int i = 0; i < va.length; i++) {
            System.out.println(va[i].toString());
            if (va[i] instanceof Auto) {
                System.out.println("Yes");
            } else System.out.println("No");
        }
    }
}
