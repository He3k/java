public class StringDemo {
    public static void main(String[] args) {
        StringBuffer sb  = new StringBuffer(64);  // add memory buffer
        sb.append("Welcome to the ");
        sb.append("club men");
        int start = 0;
        int end = 7;
        String str = sb.substring(start, end);
        System.out.println(str);
    }
}
