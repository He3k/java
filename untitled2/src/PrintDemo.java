import java.nio.file.FileAlreadyExistsException;

public class PrintDemo {
    public static void main(String[] args) throws InterruptedException {
        long mili = 100L;
        int i = 1000;
        while (i > 1) {
            System.out.println(i + " - 7 = " + (i - 7));
            Thread.sleep(mili);
            i -= 7;
        }
    }
}
